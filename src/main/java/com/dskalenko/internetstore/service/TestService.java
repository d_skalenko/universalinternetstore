package com.dskalenko.internetstore.service;

import com.dskalenko.internetstore.dao.TestDAO;
import com.dskalenko.internetstore.entity.TestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * (c) Swissquote 9/15/14
 * @author Denys Skalenko
 */
@Service
@Transactional
public class TestService {

	@Autowired
	private TestDAO testDAO;

	public TestEntity getTestEntity(String id) {
		return testDAO.find(id);
	}
}
