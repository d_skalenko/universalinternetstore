package com.dskalenko.internetstore.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * (c) Swissquote 1/23/14
 * @author Denys Skalenko
 */
@Transactional(propagation = Propagation.MANDATORY)
public abstract class AbstractDAO<T> {

	@PersistenceContext
	private EntityManager entityManager;

	protected EntityManager getEntityManager() {
		return entityManager;
	}

    public abstract T save(T entity);

	public abstract T find(String id);
}
