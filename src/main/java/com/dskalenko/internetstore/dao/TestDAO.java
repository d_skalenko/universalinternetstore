package com.dskalenko.internetstore.dao;

import com.dskalenko.internetstore.entity.TestEntity;
import org.springframework.stereotype.Repository;

/**
 * (c) Swissquote 9/15/14
 * @author Denys Skalenko
 */
@Repository
public class TestDAO extends AbstractDAO<TestEntity> {

	@Override
	public TestEntity save(TestEntity entity) {
		return null;
	}

	@Override
	public TestEntity find(String id) {
		return getEntityManager().find(TestEntity.class, id);
	}
}
