package com.dskalenko.internetstore.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * (c) Swissquote 9/15/14
 *
 * @author Denys Skalenko
 */
@Entity
@Table(name = "test_table")
@Data
public class TestEntity {

	@Id
	@Column(name = "test_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;

	@Column(name = "test_value")
	 private String test;
}
