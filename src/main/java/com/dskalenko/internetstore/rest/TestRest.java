package com.dskalenko.internetstore.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dskalenko.internetstore.entity.TestEntity;
import com.dskalenko.internetstore.service.TestService;

/**
 * (c) Swissquote 9/15/14
 * @author Denys Skalenko
 */
@Component
@Path("/test")
public class TestRest {
	@Autowired
	private TestService testService;

	@GET
	@Produces("application/json")
	@Path("/asjson/{id}/")
	public Time getTest(@PathParam("id") String id) {
		// TODO add gson lib for parse to json
		TestEntity testEntity = testService.getTestEntity(id);
		Time time = new Time();
		if (testEntity != null) {
			time.setName(testEntity.getTest());
		}
		return time;
	}

}
