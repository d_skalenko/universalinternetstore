define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('MyCtrl1', ['$scope', '$http',
        function ($scope, $http) {
            $scope.test = function () {
                $http.get('/services/timeoftheday/asjson/name"').success(function (data) {
                    $scope.time = angular.copy(data.time);
                }).error(function (data) {
                    alert('Failed to load data', data.detailMessage);
                });
            };
        }]);
});