define([
    'angular',
    'angular-route',
    './controllers/index'/*,
    './directives/index',
    './filters/index',
    './services/index' https://github.com/StarterSquad/startersquad.com/blob/master/examples/angularjs-requirejs-2/js/controllers/my-ctrl-2.js*/
], function (ng) {
    'use strict';

    return ng.module('app', [
        'app.controllers',
        'ngRoute'/*,
        'app.services',
        'app.filters',
        'app.directives'*/
    ]);
});