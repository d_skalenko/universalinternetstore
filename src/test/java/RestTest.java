import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class RestTest {
	public static void main(String[] args) {
		Client client = Client.create();

		// plain text
		WebResource r = client
				.resource("http://localhost:9090/jaxrs/services/timeoftheday/asplaintext/mathew");
		System.out.println("Plain Text=>> " + r.get(String.class));

		// json
		r = client.resource("http://localhost:9090/jaxrs/services/timeoftheday/asjson/mathew");
		client.addFilter(new HTTPBasicAuthFilter("johndoe", "password"));
		System.out.println("JSON=>> " + r.get(String.class));

		// xml
		r = client.resource("http://localhost:9090/jaxrs/services/timeoftheday/asxml/mathew");
		client.addFilter(new HTTPBasicAuthFilter("admin", "password"));
		System.out.println("XML=>> " + r.get(String.class));
	}
}
